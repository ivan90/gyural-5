<?php

/**
 * Gyural > Config
 *
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 * @version 1.10
 */

$versioning = json_decode(implode(file('sys'.DIRECTORY_SEPARATOR.'version.json')));

if($versioning->active)
	$version = $versioning->{$versioning->active};
else
	$version = $versioning->working;

if(!$version)
	die('No version specified.');

foreach($version as $key => $value) define($key, $value);

if($version->online == 0) {
	include 'sys'.DIRECTORY_SEPARATOR.'error-over.txt';
	die();
}

!defined('gyu') ? die('Error Loading configuration file.') : '';

// Self Defined Constants
if(!defined('absolute'))
	define('absolute', $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR);
define('upload', absolute . $version->uploadPath);
define('cache', absolute . cdn . 'cache' . DIRECTORY_SEPARATOR);

if(legacy == 0) {
	define('application', absolute . userFolder . DIRECTORY_SEPARATOR);
	define('applicationCore', absolute . coreFolder . DIRECTORY_SEPARATOR);
} else {
	define('application', absolute . 'app' . DIRECTORY_SEPARATOR);
	define('applicationCore', absolute . 'app' . DIRECTORY_SEPARATOR);
}

define('libs', absolute . 'libs' . DIRECTORY_SEPARATOR);

if(!defined('sendmail')) define('sendmail' , 'php');

if(!defined('uriLogin')) define('uriLogin' , $version->uri . $version->uriLogin);
if(!defined('uriUpload')) define('uriUpload' , uri . DIRECTORY_SEPARATOR . $version->uploadPath);

if(!defined('varDivider')) define('varDivider', ':'); // (override the ?key=vars always. If you want to use the standard syntax ?var=1&var2=2 etc.. replace the first ? with &)

// Regex Patterns

$dbLink_pattern = '/(.+):\/\/(\S+):(\S+)@(\S+)\/(\S+)/i';

// Database Specs

define('dbLink', $version->mysql);
define('excludeFieldPrefix', "gyu_");	// -> Set this CONSTANT to strip vars that starts with it string from being MYSQLERIALIZED
define('preventDuplicate', true);		// -> Set this to TRUE or FALSE to prevent duplicate in the CreateQuery('I', …); and ->hangExecute();

//Locate

date_default_timezone_set("Europe/Rome");
setlocale(LC_TIME, 'it_IT');