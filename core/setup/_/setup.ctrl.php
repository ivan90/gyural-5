<?php

/**
 * Setup Controller
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

class setupCtrl extends standardController {

	var $index_tollerant = true;
	var $steps = 3;

	function __construct() {
		if(!dev)
			deb_error('You can\'t launch the Setup', 1);
	}

	function GetIndex() {

		unset($_SESSION["prov"]);
		$app_data["head"]["title"] = 'Setup 1/' . $this->steps;
		
		Application('setup/_v/_commons/header', null, $app_data);
		Application('setup/_v/step1', null, $app_data);
		Application('setup/_v/_commons/footer');

	}

	function Get2() {

		$app_data["head"]["title"] = 'Setup 2/' . $this->steps;
		$app_data["error"] = $this->error;
		
		Application('setup/_v/_commons/header', null, $app_data);
		Application('setup/_v/step2', null, $app_data);
		Application('setup/_v/_commons/footer');

	}

	function Post2() {
		
		$master = json_decode(file_get_contents(absolute . 'sys/version.json'));
		
		$master->active = 'working';
		$master->working->siteName = $_POST["siteName"];
		$master->working->uri = $_POST["uri"];
		$master->working->ssl = $_POST["ssl"];
		$master->working->cdn = $_POST["cdn"];
		$master->working->mail = $_POST["email"];
		$master->working->supportMail = $_POST["email"];
		$master->working->tablesPrefix = $_POST["db_prefix"];
		$master->working->dev = $_POST["dev"];
		$master->working->db = 1;
		$master->working->sendmail = $_POST["sendmail"];
		$master->working->logStack = $_POST["logStack"];
		$master->working->IndexApp = 'index/index';
		$master->working->mysql = 'mysql://' . $_POST["db_user"] . ':' . $_POST["db_pass"] . '@' . $_POST["db_host"] . '/' . $_POST["db_name"];
		$master->working->uploadPath = $_POST["upl"];
		
		// Let's check if..
		
		# 1) upload path is writable!
		$uploadPass = is_writable(absolute . $_POST["upl"]);
		
		# 2) the database infos are right!
		if(!mysql_connect($_POST["db_host"], $_POST["db_user"], $_POST["db_pass"])) {
			$mysqlPass = mysql_error();
		} else {
			if(!mysql_select_db($_POST["db_name"]))
				$mysqlPass = mysql_error();
		}
		
		# Copy the infos.
		foreach($_POST as $k=>$v) $_SESSION[$k] = $v;
		$_SESSION["prov"] = $master;
		
		if(isset($_POST["user_username"])) {
			$_SESSION["user_username"] = $_POST["user_username"];
			$_SESSION["user_password"] = $_POST["user_password"];
		}

		if($uploadPass == true && $mysqlPass == NULL)
			$this->move('setup/Get3');
		else
			$this->move('setup/Get2', ["error" => ['uploadPass' => $uploadPass, 'mysqlPass' => $mysqlPass]]);
		
	}
	
	# Step 3
	function Get3() {
		
		$app_data["head"]["title"] = 'Setup 3/' . $this->steps;

		Application('setup/_v/_commons/header', null, $app_data);
		Application('setup/_v/step3', null, $app_data);
		Application('setup/_v/_commons/footer');
		
	}
	
	# Step 3 - Submit
	function Post3() {
		
		$simulate = false;
		
		# Step 1, create the new sys.
		file_put_contents(absolute . 'sys/'.($simulate ? 'test' : 'version').'.json', CallFunction('setup', 'json', stripslashes(json_encode($_SESSION["prov"]))));
		
		# Step 2, preare the sql
		$correctDump = str_replace('TABLE `', 'TABLE `' . $_SESSION["db_prefix"], file_get_contents(application . 'setup/_assets/dump.sql'));
		
		$conn = MysqlConnect($_SESSION["db_host"], $_SESSION["db_user"], $_SESSION["db_pass"], $_SESSION["db_name"]);
		$conn->multi_query($correctDump);

		$_SESSION["skip-1"] = true;

		header('Location: /setup/start');
		
	}

	function GetStart() {

		if(isset($_SESSION["user_username"]) && !isset($_SESSION["skip-1"])) {

			$utenteAlpha = LoadClass('users', 1);
			$utenteAlpha->setAttr('username', $_SESSION["user_username"]);
			$utenteAlpha->setAttr('password', $_SESSION["user_password"]);
			$utenteAlpha->setAttr('active', 1);
			$utenteAlpha->setAttr('group', 1);
			
			$utenteAlpha->hangExecute();

			if($utenteAlpha->id > 0)
				unset($_SESSION["user_username"]);

			session_destroy();

		}

		unset($_SESSION["skip-1"]);

		$app_data["head"]["title"] = 'Installazione Completata';
		
		Application('setup/_v/_commons/header', null, $app_data);
		Application('setup/_v/start');

		if(isset($_SESSION["user_username"])) {
			?>
			<script type="text/javascript">
			$(function() {
				location.reload();
			});
			</script>
			<?
		}

	}

	function GetVetrina() {

		$app_data["head"]["title"] = 'Setup Vetrina';
		
		Application('setup/_v/_commons/header', null, $app_data);
		Application('setup/_v/vetrina', null, $app_data);
		Application('setup/_v/_commons/footer');

	}

}