<? if(version < implode(file('http://gyural.com/version'))): ?>
<div class="alert-box warning">
	<p style="margin: 0">
		Stai per installare <strong>Gyural <? echo version; ?></strong> L'ultima versione ufficiale è la <strong><em><? echo $v = implode(file('http://gyural.com/version')); ?></em></strong><br />
		<a style="color: white" href="//gyural.com/repo" target="_blank">Scarica la versione <? echo $v; ?></a>
	</p>
</div>
<? endif; ?>



<p>
	Bene, stiamo per installare la versione <? echo version; ?> di Gyural su questo spazio web. Prima di iniziare:
</p>
<ul>
	<li><strong>Gyural</strong> funziona solo in ambienti Unix <em>(per via del modrewrite, e la gestione del filesystem.. libertà d'espansione)</em></li>
	<li><strong>Gyural</strong> necessita almeno di php 5.4 <em>(ok netsons, ok aruba, ok i cpanel in generale (distribuzioni centos/cloudlinux/debian))</em></li>
	<li><strong>Gyural</strong> necessita di un database di tipo MySql. <em>(tranne per la versione vetrina)</em></li>
</ul>
<h3 style="margin: 50px; font-size: 18px; text-align: center">Procediamo.</h3>
<center>
	<a class="button success radius small" href="/setup/vetrina">versione vetrina</a>
	<a class="button success radius" href="/setup/2">versione con database</a>
</center>