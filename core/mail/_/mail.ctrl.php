<?php

/**
 * Mail Controller
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

class mailCtrl extends standardController {
	
	var $index_tollerant = false;
	
	function CtrlIndex() {
		
		echo 'mail';
		
	}

	function GetView() {
		if($mail = LoadClass('mail', 1)->get($_GET["id"])) {
			header("Content-type: image/png");
			$mail->setAttr('delivery', time());
			$mail->putExecute();
		} else
			echo 'Ooops?';
	}

	function ApiTest() {
		CallFunction('mail', 'save', mail, 'test', 'test');
	}

}