<?php

/**
 * Api Controller
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

class apiCtrl extends standardController {

	var $index_tollerant = true;
	var $help = true;
	var $apiExcludeList = ['gyu_bucket', 'gyu_optimization', 'gyu_sdk', 'users'];
	var $format;

	function __construct() {

		$GLOBALS["api"] = true;

		if(!isset($_REQUEST["format"]))
			$_REQUEST["format"] = 'json';

		// This will work for JSON payloads.. (angular.js, for example.)
		$request_body = file_get_contents('php://input');
		$data = json_decode($request_body, true);

		if(is_array($data))
			$_REQUEST = array_merge($_REQUEST, $data);

		$this->format = $_REQUEST["format"];

		// this allow the autologin.. maybe... an hook? :)
		if(strlen($_REQUEST["token"]) > 0)
			if($user = LoadClass('users', 1)->filter(array('token', $_REQUEST["token"]), 'ONE'))
				$_SESSION["login"] = $user;

	}

	function CtrlIndex() {

		$ok = false;

		$portions = explode('/', $_SERVER["PATH_INFO"]);
		$guess = count($portions) - 1;

		if($guess == 0) {
			LoadApp('api',1)->NoInput();
			exit;
		}

		if(strstr($portions[$guess], '.')) {
			$available[] = $portions[$guess];
			$ok = true;
		}

		if(!$ok) {
			foreach($portions as $portion) {
				if(strstr($portion, '.'))
					$available[] = $portion;
			}
		}

		foreach($available as $shall) {

			$class = explode('.', $shall);
			$method = $class[count($class)-1];
			unset($class[count($class)-1]);

			$controller = implode('/', $class);

			if(isController($controller . '/' . $method))
				break;

		}

		$controller = LoadApp($controller, 1);
		$res = $controller->__exec('Api' . ucfirst($method));

		if(!$res) {
			$res["error"] = 1;
		}

		if($this->format == 'json') {
			header('Content-type: application/json; charset=utf-8');
			echo json_encode($res);
		}

		else if($this->format == 'php') {
			header('Content-type: text/plain; charset=utf-8');
			echo serialize($res);
		}

		else if($this->format == 'xml') {
			header('Content-Type: application/xml; charset=utf-8');

			$xml = new SimpleXMLElement('<root/>');
			CallFunction('api', 'to_xml', $xml, $res);
			echo $xml->asXML();

		}

	}

	function FindAllApiControllers() {

		$applications = deb_installed_app();
		foreach($applications as $app) {
			if(in_array($app["name"], $this->apiExcludeList))
				continue;

			$path = $app["isCore"] ? applicationCore : application;

			$files = (glob($path . '/' . $app["name"] . '/_/*'));
			foreach($files as $file) {
				if(strstr($file, '.ctrl.php')) {
					$validCtrls[] = $file;
				}
			}

		}

		$pattern = '/function Api(\w+)/i';

		foreach($validCtrls as $checkInside) {
			$content = file_get_contents($checkInside);
			preg_match_all($pattern, $content, $matches);
			if(count($matches[1]) > 0) {
				$portions = explode('/', $checkInside);
				list($method) = explode('.', $portions[count($portions)-1]);
				$methods[] = array('name' => $method, 'methods' => $matches[1]);
			}
		}

		return $methods;

	}

	function CtrlHelp() {

		if(!$this->help)
			$this->NoInput(1);

		$nodes = $this->FindAllApiControllers();
		
		$output[] = '<pre>';
		$output[] = '<h1>Api Help</h1>';
		$output[] = '<hr />';
		$output[] = 'Endpoint: ' . uri . '/api/[app.controller]';

		foreach($nodes as $node) {

			$output[] = '<strong>' . strtolower($node["name"]) . '</strong>';

			foreach($node["methods"] as $metodo) {

				$url = uri.'/api/'.str_replace('_', '.', $node["name"]).'.'.strtolower($metodo);

				$ref = new ReflectionClass($node["name"] . 'Ctrl');
				$function = $ref->getMethod('Api' . $metodo);

				$docBlock = $function->getDocComment();
				$args = $function->getParameters();

				$args = $function;
				$output[] = '<span style="display: inline-block; width: 50px; text-align: right">- </span>' . $metodo;
				$output[] = '<div style="margin-left: 50px"><a href="' . $url . '">'.strtolower($url).'</a>';

				$output[] = '<div style="border-radius: 2px; padding: 10px; background-color: rgba(255,255,0, .2)">'. highlight_string($args, 1) . '</div></div>';
				#$output[] = strlen($docBlock) > 0 ? "\t" . $docBlock : '';

			}

			$output[] = '<hr />';

		}

		$output[] = '<em>Gyural '.version.'</em>';

		$app_data["doc"] = implode("\n\n", $output);

		Application('/api/_v/doc', null, $app_data);


	}

	function NoInput($stop = false) {
		echo 'Sorry, no request.';
		deb_error('No Api Request.', 1);
		if($stop)
			die();
	}

}
