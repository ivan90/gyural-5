<?php

/*

----------
Gyural 1.8
----------

Filename: /app/gyu_sdk/_/gyu_sdk.ctrl.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 16/12/2013
	
*/

class gyu_sdkCtrl extends standardController {
	
	var $index_tollerant = true;
	var $format;
	
	function __construct() {
		
		if(!function_exists('deb_log')) {
			die('Debug Repository is needed!');
			return false;
		}
	}

	function GetInstaller() {

		if(!dev) deb_error('Enable DEV Mode.', 1);

		$app_data["head"]["title"] = 'Sdk / Applications';

		$installed = deb_installed_app();

		$whereInstallers = absolute . 'app-setups/';
		$installers = glob($whereInstallers . '*.zip');
		foreach($installers as $file)
			if($ccnt = CallFunction('gyu_sdk', 'inspect', $file))
				$apps[] = $ccnt;


		Application('gyu_sdk/_v/header', null, $app_data);
		
		if($apps):

		?>
		<h3><u>Applicazioni da Installare</u></h3>
		<table width="100%">
			<thead>
				<th>App</th>
				<th>File</th>
				<th>_</th>
				<th>ALR</th>
				<th></th>
			</thead>
			<tbody>
				<? foreach($apps as $app): ?>
				<? if($app["filename"]): ?>
				<tr>
					<td align="center"><? echo $app["name"]; ?></td>
					<td align="center"><small><? echo str_replace(absolute, '', $app["filename"]); ?></small></td>
					<td align="center"><? echo $app["_"] ? 'Y' : 'N'; ?></td>
					<td align="center"><? echo isApplication($app["name"]) ? '!' : 'Go!'; ?></td>
					<td><a href="/gyu_sdk/install/file:<? echo base64_encode(base64_encode($app["filename"])); ?>">Installa</a></td>
				</tr>
				<? endif; ?>
				<? endforeach; ?>
			</tbody>
		</table>
		<?
		else: 
			?>Non ci sono app da installare!!<?
		endif;
		?>
		<hr />
		<h3><u>Scarica App da gyural.com/repo</u></h3>
		<ul class="rep"><li>Attendi Caricamento…</li></ul>
		<script type="text/javascript">
		$(function() {

			$.post('/api/gyu_sdk.repo', function(repo) {
				$('.rep').html('');
				$(repo).each(function() {
					$('.rep').append('<li>'+this.name+' // <small>'+this.description+'</small> (<a href="/gyu_sdk/repo-install/id:'+this.id+'">&darr;</a>)</li>');
				});
			});

		});</script>

		<?

		Application('gyu_sdk/_v/footer');

	}

	function GetRepo_install() {
		
		if(!dev) deb_error('Enable DEV Mode.', 1);

		$id = $_GET["id"];
		$info = json_decode(implode(file('https://gyural.com/api/repo.get/id:' . $id)));

		$url = $info->publicLink;
		$path = absolute . 'app-setups/' . strtolower($info->name) . '.zip';

		CallFunction('gyu_sdk', 'download', $url, $path);
		header('Location: /gyu_sdk/installer');
		print_r($info);

	}

	function ApiRepo() {

		if(!dev) deb_error('Enable DEV Mode.', 1);

		$info = json_decode(implode(file('https://gyural.com/api/repo.list')));
		return $info;

	}

	function GetInstall() {

		if(!dev) deb_error('Enable DEV Mode.', 1);

		$file = base64_decode(base64_decode($_REQUEST["file"]));

		if($info = CallFunction('gyu_sdk', 'inspect', $file)) {
			$dest = application . $info["name"];
			$check = false;
			$go = true;

			if(!isset($_GET["force"]))
				$check = true;

			if(is_dir($dest)) {
				$go = false;
				$msg[] = 'C\'è già un app con lo stesso nome.. tutto il contenuto verrà sovrascritto.. (suggerisco di scompattare a mano e controllare a vista)';
			} else
				$msg[] = '**Non ci sono APP con lo stesso nome!';

			if(!$info["_"]) {
				$go = false;
				$msg[] = 'Non sembra avere "Underscore App"..';
			} else
				$msg[] = '**Sembra essere un "Underscore App"! :)';
			//print_r($info);
			
			if(!$check) {

				mkdir($dest);
		
				$zip = new ZipArchive;
		
				$res = $zip->open($file);
				if ($res === TRUE) {
					$zip->extractTo($dest);
					$zip->close();
					$s = 1;
				} else {
					$s = 2;
				}
		
				if($s == 1) {

					if ($handle = opendir($dest)) {
	    				while (false !== ($entry = readdir($handle))) {
	        				if ($entry != "." && $entry != "..") {
	            				$output[] = $entry;
	        				}
	    				}
	    				closedir($handle);
					}

					if(count($output) == 1 && $output[0] == $info["name"]) {
						
						if(strlen(trim($info["name"])) != 0) {
							
							$newRandName = application . md5(rand());
							rename($dest, $newRandName);

							// Now, move the content....
							rename($newRandName . '/' . $info["name"], application . $info["name"]);

							rmdir($newRandName);

						}

					}

					header('Location: /gyu_sdk/installer');
					unlink($file);

				} else
					echo 'Error.';

			} else {

				$app_data["head"]["title"] = 'Sdk / Installa';

				Application('gyu_sdk/_v/header', null, $app_data);

				echo '<ul>';
				foreach($msg as $messaggio) {
					echo '<li>'.str_replace('**', '<span style="color: green">√</span> ', $messaggio).'</li>';
				}
				echo '</ul>';

				if($go == false)
					echo '(valuta gli errori, e decidi se rischiartela o take it easy)<br /><br />';
				else
					echo 'Yooooooooo installa! installa!<br />';

				echo '<a href="/gyu_sdk/install/file:'.$_GET["file"].'/force:1">Daje! Procediamo!</a>';

				Application('gyu_sdk/_v/footer');

				die();

			}
		}

	}
	
	function GetIndex() {
		
		if(!dev) deb_error('Enable DEV Mode.', 1);

		$app_data["head"]["title"] = 'Sdk';

		Application('gyu_sdk/_v/header', null, $app_data);

		Application('gyu_sdk/_v/editor');
		
		Application('gyu_sdk/_v/footer');

	}

	function PostIndex() {

		if(!dev) deb_error('Enable DEV Mode.', 1);

		if(!isApplication($_POST["appName"])) {

			$replaces["{{app}}"] = $_POST["appName"];
			$replaces["{{ctrl}}"] = $_POST["appName"];
			$replaces["{{ctrl.clean}}"] = '';
			$replaces["{{time}}"] = date('d/m/Y');
			$replaces["{{user}}"] = $_POST["user"];
			$replaces["{{email}}"] = $_POST["email"];
			$replaces["{{pid}}"] = md5(time() * rand(0,100));

			$templateCtrl = file_get_contents(app . '/gyu_sdk/_sample/ctrl.php');
			$templateLib = file_get_contents(app . '/gyu_sdk/_sample/lib.php');
			$templateHooks = file_get_contents(app . '/gyu_sdk/_sample/hooks.php');
			$templateFuncs = file_get_contents(app . '/gyu_sdk/_sample/funcs.php');
			$versionGapp = file_get_contents(app . '/gyu_sdk/_sample/version.gapp');

			// Create the main dir.
			$keys = array_keys($replaces);
			if($_POST["global"]["lib"])
				$files[] = array($replaces["{{app}}"] . '.lib.php', str_replace($keys, $replaces, $templateLib));
			if($_POST["global"]["ctrl"])
				$files[] = array($replaces["{{app}}"] . '.ctrl.php', str_replace($keys, $replaces, $templateCtrl));
			if($_POST["global"]["hooks"])
				$files[] = array($replaces["{{app}}"] . '.hooks.php', str_replace($keys, $replaces, $templateHooks));
			if($_POST["global"]["funcs"])
				$files[] = array($replaces["{{app}}"] . '.funcs.php', str_replace($keys, $replaces, $templateFuncs));

			$files[] = array('version.gapp', str_replace($keys, $replaces, $versionGapp));

			foreach($_POST["sub"] as $v) {

				$replaces["{{ctrl.clean}}"] = '/ ' . $v["name"];

				$replaces["{{ctrl}}"] = $_POST["appName"] . '_' . $v["name"];
				if(isset($v["ctrl"])) {
					$files[] = array($replaces["{{ctrl}}"] . '.ctrl.php', str_replace($keys, $replaces, $templateCtrl));
				}

				if(isset($v["lib"])) {
					$files[] = array($replaces["{{ctrl}}"] . '.lib.php', str_replace($keys, $replaces, $templateLib));	
				}

			}

			if(count($files) > 0) {

				$base = application . $_POST["appName"];
				
				if(mkdir($base)) {
					if(mkdir($base . '/_/')) {
						foreach($files as $key => $value) {
							file_put_contents($base . '/_/' . $value[0], $value[1]);
						}
					}

					if($_POST["global"]["v"]) {
						mkdir($base . '/_v/');
					}

					if($_POST["global"]["assets"]) {
						mkdir($base . '/_assets/');
						mkdir($base . '/_assets/javascripts/');
						mkdir($base . '/_assets/css/');
						file_put_contents($base . '/_assets/css/app.less', '');
						file_put_contents($base . '/_assets/javascripts/app.js', '');
					}

				}

				header('Location: /gyu_sdk/done:1');
				
			}

		}

	}

	function ApiExists($appName) {
		
		if(!dev) deb_error('Enable DEV Mode.', 1);

		if(!$appName)
			$appName = $_REQUEST["appName"];

		if(isApplication($appName))
			return false;
		else
			return true;

	}
	
	function GetRoutes() {
		
		if(!dev) deb_error('Enable DEV Mode.', 1);
		
		$apps = deb_installed_app();
		foreach($apps as $app) {
			if($app["detail"]["isUnderscore"] == 1) {
				$_app = LoadApp($app["name"], 1);
				if(is_object($_app)) {
					#$controllers[$app["name"]]["lib"] = LoadApp($app["name"], 1);
					$methods = get_class_methods(get_class($_app));
					$validControllers = [];
					foreach($_app->methods_prefix as $single) {
						foreach($methods as $method) {
							if(stripos($method, $single) === 0) {
								$b = $app["name"] . '/' . $method;
								$validControllers[] = array($b, createUrl($b));
							}
						}
					}
					
					$controllers[$app["name"]] =  $validControllers;
					
				}
			}
		}
		echo '<pre>';
		print_r($controllers);
		echo 'Routes';
		
	}

}

?>