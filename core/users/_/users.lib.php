<?php

/*

----------
Gyural 1.8
----------

Filename: /app/users/_/users.lib.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 21/11/13

*/

class users extends standardObject {
	
	var $gyu_table = "users";
	var $gyu_id = 'id';
	var $gyu_refill = ['username', 'active', 'email'];

	function __construct() {

		if(!isset($this->id)) {
			$this->setAttr('creationTime', time());
		} else {
		}

	}

	function setPassword($password) {

		$salt = sha1(time());
		$pass = md5(sha1(md5($password).$salt));
		$toStore = $pass.'@'.$salt;
		$this->password = $toStore;

	}

	function getUserSalt() {

		if(strstr($this->password, '@')) {
			list($pass, $salt) = explode('@', $this->password);
			return $salt;
		}

	}

	function getUserPass() {

		if(!strstr($this->password, '@')) {
			list($pass, $salt) = explode('@', $this->password);
			return $pass;
		}

	}

	function isRightPass($password) {

		if(!strstr($password, '@'))
			$check = md5(sha1(md5($password).$this->getUserSalt()));
		else
			$check = $password;

		if($check . '@' . $this->getUserSalt() == $this->password)
			return true;
		else
			return false;

	}

	function _ping() {
		if($this->id) {
			$this->setAttr('lastSeen', time());
			return $this->putExecute();
		}
		return false;
	}

	function _refresh() {

		$copy = $this->get($this->id);

		return $copy->password == $this->password && $copy->active == 1 ? true : false;

	}

	function _try($username, $password) {
		
		if($user = LoadClass('users', 1)->filter(array('username', $username), 'ONE')) {
			
			$salt = $user->getUserSalt();
			
			if($user->isRightPass($password))
				return $user;
			else
				return false;

		}

	}

}


?>