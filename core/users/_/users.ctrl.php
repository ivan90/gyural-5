<?php

/*

----------
Gyural 1.8
----------

Filename: /app/users/_/users.ctrl.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: xx/xx/xx

----
Docs
----

Note: if you'r going to update a previous version of gyural and keep your users:
- Update password field in db, make it varchar(255) and then add "@" at the end of each user. [1 - why]
- Remove the /app/users and replace /funcs/autoload/security.php with the 1.5's one.

##
# 1 - Why
  -------

  This because the login mechanism is now changed, each user's password have a structure like:
  passwordsalted@salt
  the salt is a random string that will be used with the user input password to generate the "passwordsalted".
  see /app/users/_/users.lib.php and the setPassword method.

*/

class usersCtrl extends standardController {

	var $index_tollerant = false;

	function __construct() { }

	# Login #
	function GetLogin() {

		Application('users/_v/login', null);
		if(isset($this->error))
			echo '<p>'.$this->error.'</p>';
		if(logged())
			echo '<pre>' . print_r(Me(), 1) . '</pre>';

	}

	function PostLogin() {

		if(logged()) {
			$this->move('index/GetIndex');
		} else {
			$this->move('users/getLogin', array('error' => 'Username or password not valid.'));
		}

	}

	# Logout #

	function GetLogout() {
		$this->ApiLogout();
		Header('Location: /users/login/destroy:1');
	}

	# #
	# API #
	# #

	# Sample API Me #
	function ApiMe() {
		if(logged())
			return Me();
	}

	# Logout API #
	function ApiLogout() {
		if(logged()) {
			unset($_SESSION["login"]);
			return true;
		} else
			return array('error' => 1, 'string' => 'You\'r not logged');
	}

	# Api Sample #
	function ApiSample() {

		if(!isset($_REQUEST["u"]) || !isset($_REQUEST["p"]))
			return 'Specify the username and password.. ($_REQUEST[u] and $_REQUEST[p])';

		$user = LoadClass('users', 1);
		$user->setAttr('username', $_REQUEST["u"]);
		$user->setAttr('password', $_REQUEST["p"]);

		return array($user, $user->hang());

	}

}

?>