<?

if (php_sapi_name() != "cli")
    die('Can run from outside the CLI! :D' . "\n\n");

$argv = $_SERVER["argv"];
if(count($argv) < 2)
	die('Type: `php cli.php gyu_sdk help` for further help.' . "\n\n");

define('absolute', __DIR__ . DIRECTORY_SEPARATOR);
define('cli', true);

$_SERVER["PATH_INFO"] = $argv[1];

include_once(__DIR__ . '/index.php');

function cli_handler() {

	global $argv;

	$cli = new Gyu\Cli();
	$string = 'Gyural ' . version . ' - CLI';

	for($a = 0; $a < strlen($string); $a++) $aa .= '=';

	echo "\n";
	echo $cli->string(' ' . $aa . ' ', 'white', 'red') . "\n";
	echo $cli->string(' ' . $string . ' ', 'white', 'red') . "\n";
	echo $cli->string(' ' . $aa . ' ', 'white', 'red') . "\n\n";

	if(!isset($argv[2]))
		$argv[2] = 'index';
	
	$params = $argv;
	unset($params[0], $params[1], $params[2]);

	$controller = LoadApp($argv[1], 1);
	if($controller) {
		
		$controller->cli = $cli;
		$action = 'Cli' . $argv[2];
		
		echo $cli->string('Controller: ' . $argv[1], 'red', null) . "\n";
		echo $cli->string('Action: ' . $argv[2], 'red', null) . "\n\n";

		echo $cli->string('Time: ' . date('r'), 'green', null) . "\n-\n\n";

		call_user_func_array(array($controller, $action), $params);

	}

	echo "\n\n";
}