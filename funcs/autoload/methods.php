<?php

/**
 * Gyural > Funcs > Methods
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

/**
 * Create an url based on a string
 * 
 * @param  string $url
 * @param  boolean $forceOne
 * @return array
 */
function createUrl($url, $forceOne = false) {

	global $router;

	if(strstr($url, '**')) {
		return '/' . trim(str_replace('**', '', $url), '/');
	}

	$methods = array_keys($GLOBALS["env"]->methods);
	$methods[] = 'Ctrl';
	foreach ($methods as $method) {
		$mm[] = "/" . $method;
	}

	$a = 1;
	$cleanPart = str_ireplace($mm, '/', $url, $a);

	if(!stristr('/api', $url))
		$out = '/' . strtolower($cleanPart);
	else {
		$out = '/api/' . str_replace('/', '.', $cleanPart);
	}

	$reversed = $router->_reverse(strtolower($cleanPart));
	if($reversed && !$forceOne) {
		$output = array($out, $reversed, $url);
	} else {
		$output = array($out, $out, $url);
	}

	return $output;
		
}

/**
 * Retrive an Application Detail
 * 
 * @param string $application
 */
function ApplicationDetail($application) {
	
	if($info = isApplication($application)) {

		if(is_dir(application . $application))
			$dir = application . $application;
		else if(is_dir(applicationCore . $application))
			$dir = applicationCore . $application;
		else
			return false;

	}
	
	if(is_dir($dir . DIRECTORY_SEPARATOR . '_')) {
		$res["isUnderscore"] = true;
		$res["dir"] = $dir;
	} else
		$res["isUnderscore"] = false;
		
	return $res;

}

/**
 * Block the execution if the request is not HTTP_X_REQUEST
 * 
 * @return false
 */
function MethodAjax() {
	if(!isAjax())
		deb_error('This should be an ajax request.', 1);
}

/**
 * Method Standard... like ctrl keyword in controllers.
 */
function MethodStandard() {
	return true;
}

/**
 * Set as "Buffer" an application.
 * From the function call to the end of execution output will be saved on file (ttl)
 * 
 * @param integer $timeToLive
 */
function MethodBuffer($timeToLive = 31556926) {

	if(dev) return false;

	$identifier = debug_backtrace();

	if(!isset($identifier[1]["class"]))
		$id = $identifier[0]["file"];
	else
		$id = $identifier[1]["class"] . '-' . $identifier[1]["function"];	

	$uniqueId = md5($id);

	$file = cache . 'sys/buffer/' . $uniqueId;
	
	if(is_file($file)) {
		$diffTime = time() - filemtime($file);

		if($diffTime > $timeToLive) {
			unlink($file);
		} else {
			readfile($file);
			die();
		}

	} else {
		$GLOBALS["gyu_buffer"] = array($file);
		ob_start("MethodBufferSave");
	}

}

/**
 * Save the Buffer on file
 * 
 * @param string $buffer
 */
function MethodBufferSave($buffer) {

	file_put_contents($GLOBALS["gyu_buffer"][0], $buffer);
	return($buffer);

}

/**
 * Set/Retrive the HttpResponse for the current page.
 * 
 * @param integer $code
 */
function HttpResponse($code) {
	if ($code !== NULL) {
		switch ($code) {
			case 100: $text = 'Continue'; break;
			case 101: $text = 'Switching Protocols'; break;
			case 200: $text = 'OK'; break;
			case 201: $text = 'Created'; break;
			case 202: $text = 'Accepted'; break;
			case 203: $text = 'Non-Authoritative Information'; break;
			case 204: $text = 'No Content'; break;
			case 205: $text = 'Reset Content'; break;
			case 206: $text = 'Partial Content'; break;
			case 300: $text = 'Multiple Choices'; break;
			case 301: $text = 'Moved Permanently'; break;
			case 302: $text = 'Moved Temporarily'; break;
			case 303: $text = 'See Other'; break;
			case 304: $text = 'Not Modified'; break;
			case 305: $text = 'Use Proxy'; break;
			case 400: $text = 'Bad Request'; break;
			case 401: $text = 'Unauthorized'; break;
			case 402: $text = 'Payment Required'; break;
			case 403: $text = 'Forbidden'; break;
			case 404: $text = 'Not Found'; break;
			case 405: $text = 'Method Not Allowed'; break;
			case 406: $text = 'Not Acceptable'; break;
			case 407: $text = 'Proxy Authentication Required'; break;
			case 408: $text = 'Request Time-out'; break;
			case 409: $text = 'Conflict'; break;
			case 410: $text = 'Gone'; break;
			case 411: $text = 'Length Required'; break;
			case 412: $text = 'Precondition Failed'; break;
			case 413: $text = 'Request Entity Too Large'; break;
			case 414: $text = 'Request-URI Too Large'; break;
			case 415: $text = 'Unsupported Media Type'; break;
			case 500: $text = 'Internal Server Error'; break;
			case 501: $text = 'Not Implemented'; break;
			case 502: $text = 'Bad Gateway'; break;
			case 503: $text = 'Service Unavailable'; break;
			case 504: $text = 'Gateway Time-out'; break;
			case 505: $text = 'HTTP Version not supported'; break;
			default:
				exit('Unknown http status code "' . htmlentities($code) . '"');
				break;
			}

			$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
			header($protocol . ' ' . $code . ' ' . $text);
			$GLOBALS['http_response_code'] = $code;
	} else {
		$code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);
	}
	return $code;
}

/**
 * Block the execution if the request is not from the Inside
 * 
 * @return false
 */
function MethodApplication() {
	$info = debug_backtrace();
	$thisApp = $info[0]["file"];
	if(empty($GLOBALS["validApplication"][$thisApp]))
		deb_error('You can\'t call it directly', 1);
}

/**
 * Create another $env and execute it from the inside (App controller Action will be aspected)
 * 
 * @param string $app
 * @param [type] $hideOtherApplication of not null, disable recursive Applications
 * @param mixed $app_data
 */
function Application($app, $hideOtherApplication = null, $app_data = null) {
	
	if($hideOtherApplication == null) {
		if(isset($GLOBALS["hideOtherApplication"]) && $GLOBALS["hideOtherApplication"] == 1)
			return true;
	}
	
	$GLOBALS["hideOtherApplication"] = $hideOtherApplication;
	
	$GLOBALS["applicationGyural"][$app] = trim($_SERVER["PATH_INFO"], '/');
	$info = Route($app, 1);
	
	$info->calling = 1;
	$info->app_data = $app_data;
	$info->exec();

	$applicationId = uniqid();
	$GLOBALS["validApplication"][application . $app] = $applicationId;

	if(is_file(application . $app))
		include application . $app;
	else if(is_file(applicationCore . $app))
		include applicationCore . $app;

}

/**
 * Detect the method of context
 * 
 * @param string $type
 */
function MethodDetect($type = null) {
	
	$info = $_SERVER["REQUEST_METHOD"];
	
	$methods = array(	'get' => false,
						'post' => false,
						'put' => false,
						'ssl' => false,
						'delete' => false,
						'head' => false,
						'ajax' => false,
						'app' => false,
						'api' => false
					);
	
	if($info == 'GET')
		$methods["get"] = true;
	else if($info == 'POST')
		$methods["post"] = true;
	
	if(isSsl())
		$methods["ssl"] = true;
	
	if(isAjax())
		$methods["ajax"] = true;
	
	if(isset($GLOBALS["app"]) && $GLOBALS["app"] == true)
		$methods["app"] = true;
	
	if(isset($GLOBALS["api"]) && $GLOBALS["api"] == true)
		$methods["api"] = true;

	if(!$type)
		return $methods;
	else
		return $methods[$type];
	
}

/**
 * Check if the current request is from an HTTP_X_REQUEST
 * 
 * @return boolean
 */
function isAjax() {
	if(@empty($_SERVER["HTTP_X_REQUESTED_WITH"]) && @strtolower($_SERVER["HTTP_X_REQUESTED_WITH"]) != "xmlhttprequest")
		return false;
	else
		return true;
}

/**
 * Check if SSL is enabled.
 * 
 * @return boolean
 */
function isSsl() {
	if(!isset($_SERVER["HTTPS"]))
		return false;
	else if($_SERVER["HTTPS"] == 'on')
		return true;
	else
		return false;
}

/**
 * Check if the user is from a mobile device.
 * 
 * @return boolean
 */
function isMobile() {
	$is_mobile = '0';
	if(preg_match('/(android|up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
		$is_mobile = 1;
	if((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE']))))
		$is_mobile = 1;
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
	$mobile_agents = array('w3c','acs-','alav','alca','amoi','andr','audi','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','oper','palm','pana','pant','phil','play','port','prox','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr','webc','winw','winw','xda','xda-');
	
	if(in_array($mobile_ua, $mobile_agents))
		$is_mobile = 1;
	
	if (isset($_SERVER['ALL_HTTP']))
		if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini') > 0)
			$is_mobile = 1;
	
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows') > 0)
		$is_mobile = 0;
	
	return $is_mobile;
}

/**
 * Check if is a valid Application
 * 
 * @param  string $appName
 * @return boolean
 */
function isApplication($appName) {

	if(legacy == 0) {
		if(is_dir(application . $appName) || is_dir(applicationCore . $appName))
			return $appName;
	}
	else if(is_dir(application . $appName))
		return $appName;
	else
		return false;

}

if (!function_exists('getallheaders')) { 
	function getallheaders() { 
		$headers = ''; 
		foreach ($_SERVER as $name => $value)
			if (substr($name, 0, 5) == 'HTTP_')
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
		return $headers; 
	}
	
}

/**
 * Check if an application is callable (like simple php not controller/route)
 * 
 * @param string $page
 */
function IsApplicationCallable($page) {
	
	$r = null;
	if(is_dir(application . $page))
		$r = $page . "/index.php";
	else {
		if(is_file(application . $page . ".php"))
			$r = $page . ".php";
		else if(legacy == 0) {
			if(is_file(applicationCore . $page . ".php"))
				$r = $page . ".php";
		}
	}
	
	return $r;
	
}
