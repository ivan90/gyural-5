<?php

/**
 * Gyural > Funcs > Classes
 * Classes
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

/**
 * Load a class. Force the ~/libs/lib.lib.php prefixing * to the $lib
 * 
 * @param string $lib
 * @param boolean $istance
 * @param array $args
 * @param boolean $soft
 * @return mixed
 */
function LoadClass($lib, $istance = false, $args = null, $soft = false) {
	
	deb_log($lib, 'loadClass');

	$info = debug_backtrace();
	$file = $info[0]["file"];

	$remember = null;

	if(strstr($file, '.lib')) {
		list($parte) = explode('.lib', $file);
		$porzioni = explode('/', $parte);
		$remember = $porzioni[count($porzioni)-1];
	}

	if($lib[0] != '*') {
		
		if(strstr($lib, '/')) {
			list($parentLib) = explode('/', $lib);
			$nameLib = str_replace('/', '_', $lib);
		} else
			$parentLib = $nameLib = $lib;

		if(legacy == 0) {
			$specific = application . $parentLib . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR . $nameLib.'.lib.php';
			if(!is_file($specific))
				$specific = applicationCore . $parentLib . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR . $nameLib.'.lib.php';
		} else
			$specific = application . $parentLib . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR . $nameLib.'.lib.php';
		
	}
	else {
		$lib = str_replace('*', '', $lib);
		$specific = libs . $lib . ".lib.php";
		$nameLib = $lib;
	}
	
	$global = libs . $nameLib . ".lib.php";
	if(is_file($specific))
		$whereIsLocated = $specific;
	else if(is_file($global))
		$whereIsLocated = $global;

	if(is_file($whereIsLocated)) {
		
		$content = file_get_contents($whereIsLocated);
		include_once $whereIsLocated;
		
		\Gyu\Hooks::get('system.loadclass', $lib, $info, $file);

		if($istance == false)
			return $nameLib;
		else {
			$obj = new $nameLib($args);
			if(method_exists($obj, 'gyu_tablesPrefix'))
				if($obj->gyu_table) $obj->gyu_tablesPrefix();
			return $obj;
		}

	}
	
	if(!$soft)
		deb_error('Class: ' . $nameLib . 'not found.');

	return 'stdClass';
	
}

function Map($method, $array, $args) {
	if(!is_array($array))
		return 'false';
	else
		foreach($array as $key=>$res) {
			$objectRefreshed = $res;
			$objectRefreshed->$method($args);
			$return[$key] = $objectRefreshed;
		}
	return $return;
}

// alias of Map(…)
function ObjectMap($method, $array) {
	return Map($method, $array);
}
