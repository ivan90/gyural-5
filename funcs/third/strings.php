<?php

/**
 * Gyural > 3rd Funcs > Strings
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

function strings__slug($phrase, $maxLength = 50) {
	
	$subs = array('à', 'á', 'è', 'é', 'ì', 'í', 'ò', 'ó', 'ù', 'ú');
	$rep  = array('a', 'a', 'e', 'e', 'i', 'i', 'o', 'o', 'u', 'u');

	$result = str_replace($subs, $rep, strtolower($phrase));
	
	$result = preg_replace("/[^a-z0-9\s-]/", "", $result);
	$result = trim(preg_replace("/[\s-]+/", " ", $result));
	$result = trim(substr($result, 0, $maxLength));
	$result = preg_replace("/\s/", "-", $result);
	
	return $result;

}

function strings__substr($stringa, $length, $append = '…') {
	
	if(strlen($stringa) > $length)
		return substr($stringa, 0, $length) . $append;
	
	return $stringa;
	
}

function strings__taglia($testo, $word, $append = '…') {

	if(!is_numeric($word)||($word < 1)) {
		$word = 100;
	}

	$newtext = wordwrap($testo, $word, " | ", true);
	
	$new = explode(' | ', $newtext);
	$txt_return = $new[0];
	if(count($new) > 1) {
		$txt_return .= ' ' . $append;
	}

	return $txt_return;

}

function strings__taglia2($testo,$numMaxCaratteri){
	
	$testo = ereg_replace("&nbsp;","",$testo);
	$contenuto = substr(strip_tags(trim($testo)),0,$numMaxCaratteri);
	$arr = explode(" ",$contenuto);
	$contenuto = "";

	for($u=0; $u<count($arr)-1; $u++) { $contenuto .= $arr[$u]." "; }
	$contenuto = substr($contenuto,0,strlen($contenuto)-1);
	
	return $contenuto;
	
}


function strings__random($length = 8) {

	$times = ceil($length / 25);
	$actual = time() * mt_rand(0, time());
	$outString = "";
	for($i = 0; $i < $times; $i++) {
		$actual = md5($actual);
		$outString .= $actual;
	}
	
	return strtoupper(substr($outString, 0, $length));

}
