<?php

/**
 * Gyural > 3rd Funcs > Date
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

function date__toUnix($date) {	
	$pattern = '/(\d+)/i';
	preg_match_all($pattern, $date, $vettore);
	$date = mktime($vettore[0][3], $vettore[0][4], 0, $vettore[0][1], $vettore[0][0], $vettore[0][2]);
	return $date;
}

function date__toDay($seconds) {
	return ceil($seconds/86400);
}

function date__toDays($seconds) {
	CallFunction('date', 'toDay', $seconds);
}

function date__ago($data) {

	if($data > time())
		$secs = $data-time();
	else
		$secs = time()-$data;

    $bit = array(
        'year'        => $secs / 31556926 % 12,
        'week'        => $secs / 604800 % 52,
        'day'        => $secs / 86400 % 7,
        'hour'        => $secs / 3600 % 24,
        'minute'    => $secs / 60 % 60,
        'second'    => $secs % 60
        );
        
    foreach($bit as $k => $v)
        if($v > 0)$ret[] = $v . $k;

    array_splice($ret, count($ret)-1, 0, '');
    #$ret[] = 'ago.';
    if(!$ret)
    	return 'now';
    return join(' ', $ret);

}

// Check if $year is leap.
function date__leapYear($year = null) {
	
	if($year == null)
		$year = date('Y');

	$is_leap = date('L', strtotime("$year-1-1"));

	return $is_leap;

}

function date__Easter($year = null) {

	if($year == null)
		$year = date('Y');

	#$year = date("Y", $date); 
	$r1 = $year % 19; 
	$r2 = $year % 4; 
	$r3 = $year % 7; 
	$ra = 19 * $r1 + 16; 
	$r4 = $ra % 30; 
	$rb = 2 * $r2 + 4 * $r3 + 6 * $r4; 
	$r5 = $rb % 7; 
	$rc = $r4 + $r5; 
	//Orthodox Easter for this year will fall $rc days after April 3 
	return strtotime("3 April $year + $rc days");

} 
