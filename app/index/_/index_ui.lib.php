<?php

class index_ui extends gyu_lumberjack {

	function __construct() {

		parent::__construct();
		$this->config["dev"] = false;
		#$this->config["dev"] = true;
		$this->config["theme"] = 'app/index';

	}

	function __top($config = null) {

		/* JAVASCRIPT */
		$this->addJavascript('http://code.jquery.com/jquery-1.11.0.min.js');
		$this->addJavascript('app/index/_assets/javascripts/app.js');

		/* CSS */

		$this->addCss('http://cdn.foundation5.zurb.com/foundation.css');
		$this->addCss('app/index/_assets/css/style.less');

		parent::__top();

	}

}

?>