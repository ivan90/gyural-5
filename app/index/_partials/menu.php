<div class="sb">
	<label>TABLE OF CONTENTS</label>
	<hr />
	<ul class="no-bullet">
		<li>
			<strong><a href="<?=createUrl('index/GetIndex/page:101')[1];?>">Getting Started</a></strong>
			<ul>
				<li><a href="<?=createUrl('index/GetIndex/page:101-install')[1];?>">Installazione</a></li>
				<li><a href="<?=createUrl('index/GetIndex/page:101-config')[1];?>">Configurazione</a></li>
				<li><a href="<?=createUrl('index/GetIndex/page:101-routing')[1];?>">Routing</a></li>
				<li><a href="<?=createUrl('index/GetIndex/page:101-requests')[1];?>">Richieste e Risposte</a></li>
				<li>
					<a href="<?=createUrl('index/GetIndex/page:patterns')[1];?>">Pattern</a>
					<ul>
						<li><a href="<?=createUrl('index/GetIndex/page:patterns-underscore')[1];?>">Underscore APPs (M<em>V</em>C)</a></li>
						<li><a href="<?=createUrl('index/GetIndex/page:patterns-standard')[1];?>">Standard (PHP4)</a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li>
			<strong><a href="<?=createUrl('index/GetIndex/page:arch')[1];?>">Architettura</a></strong>
			<ul class="no-bullet">
				<li>
					<a href="<?=createUrl('index/GetIndex/page:arch-underscore')[1];?>">Underscore APP</a>
					<ul>
						<li><a href="<?=createUrl('index/GetIndex/page:arch-underscore-controller')[1];?>">Controller</a></li>
						<li><a href="<?=createUrl('index/GetIndex/page:arch-underscore-model')[1];?>">Model</a></li>
						<li><a href="<?=createUrl('index/GetIndex/page:arch-underscore-funcs')[1];?>">Functions</a></li>
						<li><a href="<?=createUrl('index/GetIndex/page:arch-underscore-hooks')[1];?>">Hooks</a></li>
						<li><a href="<?=createUrl('index/GetIndex/page:arch-underscore-subs')[1];?>">Sub_APP</a></li>
					</ul>
				</li>
				<li><a href="<?=createUrl('index/GetIndex/page:arch-libs')[1];?>">Libs</a></li>
				<li><a href="<?=createUrl('index/GetIndex/page:arch-funcs')[1];?>">Functions</a></li>
				<li><a href="<?=createUrl('index/GetIndex/page:arch-psr')[1];?>">Vendors (PSR)</a></li>
			</ul>
		</li>
	</ul>
	<hr />
	<ul class="no-bullet">
		<li><a href="<?=createUrl('index/GetIndex/page:manifesto')[1];?>">Manifesto</a></li>
	</ul>	
</div>