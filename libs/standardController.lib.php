<?php

/**
 * Gyural > standardController
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */

class standardController {

	/**
	 * Methods of calling
	 * @var array
	 */
	var $methods_prefix = array('App', 'Get', 'Ajax', 'Ctrl', 'Post', 'Put', 'Head', 'Api', 'Cli');

	/**
	 * If there's no candidate for the request, use the index?
	 * (false, by default)
	 * @var boolean
	 */
	var $index_tollerant = false;

	var $secure_requests = true;

	/**
	 * Array of filters (before the Action)
	 * @var null
	 */
	var $gyu_filterBefore = null;

	/**
	 * Array of filters (after the Action)
	 * @var null
	 */
	var $gyu_filterAfter = null;

	/**
	 * Mantain the move params after the move?
	 * (false by default)
	 * @var null
	 */
	var $gyu_preserveMove = false;

	/**
	 * Move the request to another Controller Action
	 * 
	 * @param  string $to The location
	 * @param  array Attributes to pass to $to (retrived by $this->)
	 * @param  integer $code HTTP response code
	 * @return false
	 */
	public function move($to, $with = null, $code = 200) {
		
		$redirectTo = createUrl($to);

		if(is_array($redirectTo)) {
			$to = $redirectTo[2];	
			$redirectTo = $redirectTo[1];
		}

		$_SESSION["move-to"][$to] = $with;
		HttpResponse($code);
		header('Location: ' . $redirectTo);

	}

	/**
	 * Add a Filter to a Controller Action
	 * 
	 * @param  string $type Before or After
	 * @param  mixed $what Action to perfor to determine if the filter is true or false
	 * @param  integer $stop If 1/true if the filter fail stop the execution
	 * @param  string $redirect If stop = 1, move to $redirect
	 * @param  array $strict Array of methods that use that filter.
	 * @return [type]
	 */
	function gyuFilter($type, $what, $stop = 0, $redirect = '/', $strict = array()) {

		if($type == 'before')
			$stack = 'gyu_filterBefore';
		else if($type == 'after')
			$stack = 'gyu_filterAfter';
		else
			return false;

		$this->{$stack}[] = array($what, $stop, $redirect, $strict);

	}

	/**
	 * @param  string $type Before or After
	 * @param  mixed $method Action
	 * @return true
	 */
	function gyuFilterApply($type, $method) {

		if(is_array($this->$type)) {
			foreach($this->$type as $k => $singleItem) {
				if(count($singleItem[3]) > 0)
					if(!in_array(strtolower($method), array_map('strtolower', $singleItem[3])))
						continue;
				if(is_callable($singleItem[0])) {
					$res = $singleItem[0]($this);
				} else if(is_string($singleItem[0])) {
					list($app, $action) = explode('.', $singleItem[0]);
					$res = LoadApp($app, 1)->{$action}($this);
				}

				if($res != false && $singleItem[1] == 1) {
					$this->move($singleItem[2], $res);
					die();
				} else {
					$this->{$type}[$k]["result"] = true;
				}
			}
		}

		return true;
			
	}

	/**
	 * Check if a $parts is a valid Controller Action
	 * 
	 * @param  string $parts
	 * @return mixed
	 */
	function __haveControllerAutotest($parts) {

		$realParts = explode('/', $parts);
		if($realParts[0] == $this->__className())
			unset($realParts[0]);

		$realParts2 = array_values($realParts);

		$realIndex = count($realParts);
		foreach($realParts2 as $index => $part) {
			$string = '';
			for($current = $index; $current < $realIndex; $current++) {
				$string .= '/' . $realParts2[$current-$index];
			}
			$portions[] = $string;
		}

		foreach($portions as $subCtrlCheck) {
			$parts = explode('/', $subCtrlCheck);
			$gP = null;
			foreach($parts as $part) {
				if(trim(strlen($part)) > 0)
					$gP[] = $part;
			}
			$latest = $gP[count($gP)-1];
			unset($gP[count($gP)-1]);
			$toTest[] = '/' . implode('_', $gP) . (strlen(implode('_', $gP)) > 0 ?'/':'') . $latest;
		}

		$portions = array_merge($toTest, $portions);
		$portions = array_unique($portions);

		foreach($portions as $v) {
			if($a = $this->__haveController($v))
				return $a;
		}

		return false;

	}

	/**
	 * Retrive the name of the Controller (without Ctrl suffix)
	 * 
	 * @return string
	 */
	function __className() {
		return str_replace('Ctrl', '', get_class($this));
	}

	/**
	 * Check if $parts method exists
	 * 
	 * @param  string $parts
	 * @return mixed
	 */
	function __haveController($parts) {

		if(!is_array($parts))
			$parts = explode('/', $parts);

		foreach($parts as $ipd => $parte) {
			if(strlen($parte) == 0)
				unset($parts[$ipd]);
		}
		reset($parts);
		$firstItem = key($parts);

		if($parts[$firstItem] == $this->__className())
			unset($parts[$firstItem]);

		$availableControllers = null;
		$controllerName = str_replace('-', '_', implode('__', $parts));

		foreach($this->methods_prefix as $method) {

			$methodTrying = $method . ucfirst($controllerName);

			if(method_exists($this, $methodTrying))
				$availableControllers[$method] = get_class($this) . '/' . $methodTrying;

		}

		return($availableControllers);

	}

	/**
	 * @param  string $method Name of Controller Action
	 * @param  unused $type
	 * @return mixed
	 */
	function __exec($method, $type) {

		$controllerName = str_ireplace('ctrl', '', get_class($this));
		if(@is_array($_SESSION["move-to"][$controllerName . '/' . $method]) || @is_object($_SESSION["move-to"][$controllerName . '/' . $method])) {
			foreach($_SESSION["move-to"][$controllerName . '/' . $method] as $k => $v) {
				$this->$k = $v;
			}
			if($this->gyu_preserveMove == false)
				unset($_SESSION["move-to"][$controllerName . '/' . $method]);
		}

		$this->gyuFilterApply('gyu_filterBefore', $method);
		
		$res = $this->$method();

		$this->gyuFilterApply('gyu_filterAfter', $method);

		if($res)
			return $res;

		return uniqid();

	}

}
