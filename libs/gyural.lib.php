<?php

/**
 * Gyural > Kerner Lib ($env)
 *
 * @version 1.10
 * @author Federico Quagliotto <f.quagliotto@mandarinoadv.com>
 */


class gyural {

	/**
	 * Starting Time
	 * @var null
	 */
	var $creationTime = null;

	/**
	 * User Session
	 * @var null
	 */
	var $userSession = null;

	var $methods = array();
	var $availableControllers = false;

	function __construct($methods) {

		$this->url = rtrim($_SERVER["REQUEST_URI"], '/');
		$this->creationTime = time();
		$this->userSession = Me();
		$this->methods = $methods;
		$this->session = $_SESSION;
		$this->requests = array(
			'get' => $_GET,
			'post' => $_POST,
			'request' => $_REQUEST,
			'files' => $_FILES
		);
		$this->server = $_SERVER;

	}

	/**
	 * Debug the current environment
	 * 
	 * @return object
	 */
	function debug() {

		echo '<pre>' . print_r($this, 1) . '</pre>';
		return $this;

	}

	/**
	 * Check if a controller exists. (LoadController alias)
	 * 
	 * @param  string $controllerName
	 * @return mixed
	 */
	function controllerExists($controllerName) {

		return(LoadController($controllerName));

	}

	/**
	 * LoadController Alias
	 * 
	 * @param object
	 */
	function Controller($controllerName) {
		return LoadController($controllerName, 1);
	}

	/**
	 * Check if the $method is callable from the context
	 * 
	 * @param array $args
	 * @param array $methods
	 */
	function IsCallable($args, $methods) {

		if($methods["app"] == true) {
			if(isset($args["App"])) {
				$controller = $args["App"];
				$type = 'App';
			}
		} else if($methods["ajax"] == true) {
			if(isset($args["Ajax"])) {
				$type = 'Ajax';
				$controller = $args["Ajax"];
			}
		} else if($methods["get"] == true) {
			if(isset($args["Get"])) {
				$type = "Get";
				$controller = $args["Get"];
			}
		} else if($methods["post"] == true) {
			if(isset($args["Post"])) {
				$controller = $args["Post"];
				$type = "Post";
			}
		}

		if(!isset($controller)) {
			if(isset($args["Ctrl"])) {
				$controller = $args["Ctrl"];
				$type = 'Ctrl';
			}
		}

		if(!isset($controller))
			return false;

		return $controller;

	}

	/**
	 * Exec the enviroment
	 * 
	 * @return false
	 */
	function exec() {

		$controllers = false;
		$standard = false;

		if(isset($this->internal_execution)) {
			$func = $this->candidate;
			if(is_callable($func))
				return $func();
		}

		if($this->availableControllers) {

			list($Controller, $Method) = explode('/', $this->availableControllers);

			$clone = clone $this;

			$clone->execution = LoadController($Controller, 1);
			$clone->execution->gyu_environment = $this;

			if(isset($this->gyu_filters)) {
				foreach($this->gyu_filters as $filter) {
					$clone->execution->gyuFilter($filter[0], $filter[1], $filter[2], $filter[3]);
				}
			}

			return $clone->execution->__exec($Method, @$type);

		} else if(strlen($this->availableApplication) > 0) {

			if(isset($this->app_data))
				$app_data = $this->app_data;

			if(is_file(application . $this->availableApplication))
				include application . $this->availableApplication;
			else if(legacy == 0)
				include applicationCore . $this->availableApplication;

		} else {

			if($this->availableControllersError) {

				list($Controller, $Method) = explode('/', $this->availableControllersError);

				$clone = clone $this;

				$clone->execution = LoadController($Controller, 1);
				$clone->execution->gyu_environment = $this;

				HttpResponse(404);

				return $clone->execution->__exec($Method, $type);

			} else if(strlen($this->availableApplicationError) > 0) {

				if(isset($this->app_data))
					$app_data = $this->app_data;

				HttpResponse(404);
				if(is_file(application . $this->availableApplicationError))
					include application . $this->availableApplicationError;
				else
					include applicationCore . $this->availableApplicationError;

			}

		}

	}

}
