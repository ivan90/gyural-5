<?php

/*

------------
Gyural 1.9.1
------------

Filename: /libs/router.lib.php
 Version: 1.9.1
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 04/11/2014

*/

class router {

	var $routes;

	/*

	---
	map
	---

	map(match, direction, code);

	*/

	function map($match, $direction, $code = 200) {

		$match = '/^'.str_replace(array('#', '/'), array('(.+)', "\/"), $match) . '/';
		$this->routes[$match] = array($direction, $code);

	}

	/*

	-----
	b_map
	-----

	*/

	function b_map($match, $direction, $code = 200) {

		$regex = '/(:|;)(\w+)/i';

		preg_match_all($regex, $match, $matches);

		if(is_array($matches[1])) {
			$methods = $matches[1];
			$varNames = $matches[2];
		}

		$vec_From[] = '/';
		$vec_Repl[] = '\/';

		foreach($varNames as $index => $v) {
			$toReplace = $methods[$index] . $v;
			$with = '(\S+)' . ($methods[$index] == ':' ? '' : '?');
			$vec_From[] = $toReplace;
			$vec_Repl[] = $with;
		}


		$match_converted = '/^'.str_replace($vec_From, $vec_Repl, $match).'/';

		$this->routes[$match_converted] = array($direction, $code, $vec_From);

	}

	/*

	----
	_try
	----

	*/

	function _try($page) {

		foreach($this->routes as $pattern => $action) {

			if(preg_match($pattern, $page)) {
				preg_match_all($pattern, $page, $res);
				$out = $action[0];
				$code = $action[1];

				if(count($action) == 3) {
					foreach($action[2] as $k => $v) {
						if($k > 0) {
							$rep[] = $v;
							$sos[] = str_replace(array(':', ';'), '', $v) . ':' . $res[$k][0];
						}
						$out = str_replace($rep, $sos, $out);
					}

				} else {
					unset($res[0]);
					foreach($res as $k)
						$out = preg_replace('/#/', $k[0], $out, 1);
				}

				return $out;

			}

		}

		return $page;

	}

	/*

	-----
	debug
	-----

	*/

	function debug() {

		echo '<pre>';
		print_r($this->routes);
		echo '</pre>';

	}

}

?>